#include "ContiguousBlockStorage.h"
#include "logger.h"

static float blockstorage[CONTIGUOUSSTORAGE_NUM_BLOCKS*2][CONTIGUOUSSTORAGE_BLOCK_SIZE];

static int newest_block = CONTIGUOUSSTORAGE_NUM_BLOCKS;

void ContiguousBlockStorage_StoreBlock(float * block)
{
  ASSERT(newest_block >= CONTIGUOUSSTORAGE_NUM_BLOCKS);
  //store 2 copies of the block for rotate-ability
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    blockstorage[newest_block][i] = block[i];
    blockstorage[newest_block-CONTIGUOUSSTORAGE_NUM_BLOCKS][i] = block[i];
  }

  newest_block++;

  if(newest_block >= CONTIGUOUSSTORAGE_NUM_BLOCKS*2){
    newest_block = CONTIGUOUSSTORAGE_NUM_BLOCKS;
  }
}
float * ContiguousBlockStorage_Beginning(void)
{
  return (float *) blockstorage[newest_block-CONTIGUOUSSTORAGE_NUM_BLOCKS];
}

uint32_t ContiguousBlockStorage_BufferSize()
{
  return CONTIGUOUSSTORAGE_BLOCK_SIZE*CONTIGUOUSSTORAGE_NUM_BLOCKS;
}


//for testing
#include <stdio.h>
#include "logger.h"
void ContiguousBlockStorage_Print(void)
{
  for(int i=0;i<8;i++){
    Logger_Printf("\n%d\n",i);
    for(int j=0;j<16;j++){
      Logger_Printf("%d: %f\n", i*j, ((float*)blockstorage)[i*16+j]);
    }
  }
}
void ContiguousBlockStorage_PrintTheWholeBuffer(void)
{
  float * beginning = ContiguousBlockStorage_Beginning();

  for(int i=0;i<4*CONTIGUOUSSTORAGE_BLOCK_SIZE;i++){
      Logger_Printf("%d: %f\n", i, beginning[i]);
    }
}

