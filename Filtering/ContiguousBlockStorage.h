#include <stdint.h>

#define CONTIGUOUSSTORAGE_BLOCK_SIZE 256
#define CONTIGUOUSSTORAGE_NUM_BLOCKS 16

void ContiguousBlockStorage_StoreBlock(float * block);
float * ContiguousBlockStorage_Beginning(void);
uint32_t ContiguousBlockStorage_BufferSize();

//for testing

void ContiguousBlockStorage_Print(void);
void ContiguousBlockStorage_PrintTheWholeBuffer(void);
