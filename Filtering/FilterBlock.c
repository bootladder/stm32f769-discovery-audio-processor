#include "FilterBlock.h"
#include <string.h>
#include "logger.h"
#include "tinyprintf.h"
#include "steve_global.h"
#include "helpers.h"
#include "steve_lcd_logger.h"

// Defined externally by user.  This pointer is set by user to allow sanity checks
#define NUMBER_OF_FILTER_BLOCKS 20
FilterBlock_t * ArrayOfFilterBlocks;

void FilterBlock_SetCutoffFrequency(FilterBlock_t * self, uint8_t freq){
  self -> cutoff_freq = freq;
}
void FilterBlock_SetBandwidth(FilterBlock_t * self, uint8_t bandwidth){
  self -> bandwidth = bandwidth;
}
void FilterBlock_SetNumFilterTaps(FilterBlock_t * self, uint8_t taps){
  self -> numFilterTaps = taps;
}
void FilterBlock_SetEnabled(FilterBlock_t * self, uint8_t enabled){
  self -> enabled = enabled;
}
void FilterBlock_Enable(FilterBlock_t * self, uint8_t dontcare){
  (void)dontcare;
  ASSERT(FilterBlock_IsValidInstance(self));
  Logger_Debug( "\n\ENABLE!!!!\n");
  self -> enabled = 1;
}
void FilterBlock_Disable(FilterBlock_t * self, uint8_t dontcare){

  (void)dontcare;
  ASSERT(FilterBlock_IsValidInstance(self));

  char str[20] = "";
  tfp_snprintf(str, 20, "%p", self);
  Logger_Debug(str);
  Logger_Debug( "\nDISABLE!!!!\n");
  self -> enabled = 0;
}

//
//// set this equal to something
//compute_function_ptr_t compute_function_ptr;
//
void FilterBlock_SetComputeFunction(FilterBlock_t * self, compute_function_ptr_t f)
{
  self -> computeCoefficientsFunc = f;
}

//call out to a function for this, copy the coefficients into self
void FilterBlock_ComputeFilterCoefficients(FilterBlock_t * self)
{
  float * coefs =  self->computeCoefficientsFunc(self->numFilterTaps,
                                       self->cutoff_freq,
                                       self->bandwidth);

  for(int i=0; i < self->numFilterTaps; i++){
    self->filterCoefficients[i] = coefs[i];
  }
}


// For each non-zero entry in the list, call MIDIMap_Add on each one
void FilterBlock_RegisterMIDIMap(FilterBlock_t * self)
{
  ASSERT(FilterBlock_IsValidInstance(self));
  for(int i=0;i<10;i++){ // hard coded 10 here
    if(self->midiMapEntries[i].message.status == 0)
      continue;  //terrible way to skip null entries
    MIDIMap_Add(self, &(self->midiMapEntries[i]));

  }
}

float * FilterBlock_ApplyFilter(FilterBlock_t * self, float * buf, uint32_t buffer_size)
{
  if( 0 == (self -> enabled))
    {
      return buf;
    }

  float * out = self -> filter(self, buf, buffer_size);
  return out;
}



//Check if the pointer is in the table of Filter Blocks
//Used for sanity check asserts
bool FilterBlock_IsValidInstance(FilterBlock_t * self)
{
  for(int i=0;i<NUMBER_OF_FILTER_BLOCKS;i++)
    {
      if(&ArrayOfFilterBlocks[i] == self)
        return true;
    }
  return false;
}

void FilterBlock_SetPointerToFilterBlockArray(FilterBlock_t * blocks)
{
  ArrayOfFilterBlocks = blocks;
}


char * FilterBlock_ToString(FilterBlock_t * self)
{
  static char str[1000];
  static char fmt[1000] =
    "FilterBlock : %p\n"
    "enabled : %d\n"
    ;

  tfp_snprintf(str, 1000, fmt,
               self,
               self->enabled
               );
  return str;
}

char * FilterBlock_CoefficientsToString(FilterBlock_t * self)
{
  static char one_coefficient[100] = "hello\n\n\n";
  for(int i=0; i<MAX_NUM_FILTER_COEFFCIENTS;i++){
    float2string(self->filterCoefficients[i], one_coefficient);
    Logger_Debug(one_coefficient);
    Logger_Debug("\n");

  }
  return one_coefficient;
}


void FilterBlock_LogDebug(FilterBlock_t * self, char * str)
{
  Steve_LCD_Logger_DisplayStringAtLine(str, self->logging_id);
}
