#ifndef __FILTERBLOCK_H_
#define __FILTERBLOCK_H_

#include <stdint.h>
#include "MIDIMap.h"

#define AUDIO_BUFFER_SIZE 1024
#define MAX_NUM_FILTER_COEFFCIENTS 200

struct block;
typedef struct block FilterBlock_t;

typedef float * (* FilterBlockFunctionPointer_t)(FilterBlock_t * t, float * a, uint32_t buffer_size);
typedef void (*FilterBlockMIDICallback_t)(FilterBlock_t * self, uint8_t value);
typedef float * (*compute_function_ptr_t)(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth);

typedef struct block {
  uint8_t enabled;
  uint8_t volume;
  uint32_t cutoff_freq;
  uint32_t bandwidth;
  uint16_t numFilterTaps;
  float filterCoefficients[MAX_NUM_FILTER_COEFFCIENTS];

  //MIDI_Message_t midi_message;
  //FilterBlockMIDICallback_t midi_callback;
  MIDIMapEntry_t midiMapEntries[10];


  compute_function_ptr_t computeCoefficientsFunc;
  FilterBlockFunctionPointer_t filter;

  uint8_t logging_id;

} FilterBlock_t;


//control interface
void FilterBlock_SetCutoffFrequency(FilterBlock_t * self, uint8_t freq);
void FilterBlock_SetBandwidth(FilterBlock_t * self, uint8_t freq);
void FilterBlock_SetNumFilterTaps(FilterBlock_t * self, uint8_t taps);
void FilterBlock_SetEnabled(FilterBlock_t * self, uint8_t enabled);
void FilterBlock_Enable(FilterBlock_t * self, uint8_t dontcare);
void FilterBlock_Disable(FilterBlock_t * self, uint8_t dontcare);


void FilterBlock_SetComputeFunction(FilterBlock_t * self, compute_function_ptr_t f);
void FilterBlock_ComputeFilterCoefficients(FilterBlock_t * self);

float * FilterBlock_IdealBandpassFilter(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth);

void FilterBlock_RegisterMIDIMap(FilterBlock_t * self);

float * FilterBlock_ApplyFilter(FilterBlock_t * self, float * buf, uint32_t buffer_size);


bool FilterBlock_IsValidInstance(FilterBlock_t * self);
void FilterBlock_SetPointerToFilterBlockArray(FilterBlock_t * blocks);

char * FilterBlock_ToString(FilterBlock_t * self);
char * FilterBlock_CoefficientsToString(FilterBlock_t * self);

void FilterBlock_LogDebug(FilterBlock_t * self, char * str);

#endif
