#include "FilterFunc_FFT.h"
#include "stm32f769xx.h"
#include "arm_math.h"
#include "logger.h"
#include <stdlib.h>  //for abs

#define MAXIMUM_FFT_SIZE 4096
static float originalBufCopy[MAXIMUM_FFT_SIZE];
static float fftResult[MAXIMUM_FFT_SIZE];
static float fftMagnitudes[MAXIMUM_FFT_SIZE/2];  //convert complex result to magnitude
static arm_rfft_fast_instance_f32 S;

//find the peak (fundamental?)
//don't look higher than range
static int binRange = 256;
static int max = 0;
static int max_index = 0;

int FilterFunc_FFT_GetMaxFrequencyIndex(void){ return max_index; }
int FilterFunc_FFT_GetMaxFrequencyValue(void){ return max; }


//FFT
// real FFT output alternates real[0], imag[0], real[1], imag[1] .. N/2
// so the array of size N stores N/2 complex numbers, for freqs 0 to N/2
// so, take the complex magnitude of those.
float * FilterFunc_FFT(float * buf, uint32_t buffer_size){

  //Copy the buffer because FFT modifies it
  for(uint32_t i=0; i<buffer_size; i++){
    originalBufCopy[i] = buf[i];
  }

  arm_rfft_fast_init_f32(&S, buffer_size);
  arm_rfft_fast_f32(&S, originalBufCopy, fftResult, 0);  //0 for fft
  arm_cmplx_mag_f32(fftResult, fftMagnitudes, buffer_size/2);

  max = 0;
  for(int i=0; i<binRange; i++){
    if( abs(fftMagnitudes[i]) > max){
      max = (int)fftMagnitudes[i];
      max_index = i;
    }
  }
  max = max / buffer_size; //scale it down


  #define PRINT_FFT_NOT
  #ifdef PRINT_FFT
  Logger_Debug("\nMax Value: ");
  Logger_Debug_Int(max);
  Logger_Debug("Max Index: ");
  if(max_index > binRange)
    max_index = binRange;
  for(int i=0;i<max_index;i++){
    Logger_Debug("-");
  }
  #endif

  return buf;  //doesnt matter what is returned
}
