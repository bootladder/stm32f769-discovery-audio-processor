#include "FilterBlock.h"
#include <stdint.h>

float * FilterFunc_FFT(float * buf, uint32_t buffer_size);

int FilterFunc_FFT_GetMaxFrequencyIndex(void);
int FilterFunc_FFT_GetMaxFrequencyValue(void);
