#include "FilterFunc_FIR.h"
#include "stm32f769xx.h"
#include "arm_math.h"

////////////////////////////////////////////////////////////////////////////////////////

static arm_fir_instance_f32 S;
#define SAMPLE_FREQUENCY 16000.0
#define MAX_BLOCK_SIZE 1024
#define MAX_NUM_TAPS 1024

float * MyFilterBlocks_ComputeCoefficients_IdealBandpassFilter(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth)
{
  (void)bandwidth;

  static float coeffs[MAX_NUM_FILTER_COEFFCIENTS];

  //float A = (float)2.0*3.14*cutoff/48000.0; //normalized frequency

  float fc_normalized = (float)cutoff / SAMPLE_FREQUENCY;

  float coefficient_sum = 0.0;
  for(int i=0; i<numTaps;i++)
    {
      int n = i - (numTaps/2);  //apply a time shift of numTaps/2
      if(n==0){
        coeffs[i] = 1;
        coefficient_sum += coeffs[i];
        continue;
      }


      //coeffs[i] = arm_sin_f32(A*(float)n)  /  (A*(float)n);

      coeffs[i] = fc_normalized * arm_sin_f32(3.14*fc_normalized*(float)n)  /  (3.14*fc_normalized*(float)n);

      coefficient_sum += coeffs[i];
    }

  //coeffs[0] = (A) - (B);
  //float coefficient_sum = coeffs[0];


  //for(int i=1;i<numTaps;i++){
  //  coeffs[i] = (A*sin(A*(float)i)/(A*(float)i)) - (B*sin(B*(float)i)/(B*(float)i));
  //  coefficient_sum += coeffs[i];
  //}

  for(int i=0;i<numTaps;i++){
    coeffs[i] = coeffs[i] / coefficient_sum;
  }



  //test:  rewrite coeffs
  //for(int i=0;i<numTaps;i++){
  //  coeffs[i] = 1.0 / (float)numTaps;
  //}


  //new plan:  init the struct here
  static float firStateF32[MAX_BLOCK_SIZE + MAX_NUM_TAPS - 1];
  arm_fir_init_f32(&S, numTaps, coeffs, &firStateF32[0], 256); //blocksize 256


  return coeffs;
}




float * filter_fir_bandpass(FilterBlock_t * self, float * buf, uint32_t buffer_size){

  uint32_t blockSize = buffer_size;  //process the whole buffer of samples

  static float outputFloatBuffer[AUDIO_BUFFER_SIZE]; //max size


  arm_fir_f32(&S, buf, outputFloatBuffer, blockSize);

  for(uint32_t i=0;i<buffer_size;i++)
    buf[i] = outputFloatBuffer[i];
  return buf;
}
