#include "FilterBlock.h"
#include <stdint.h>

float * MyFilterBlocks_ComputeCoefficients_IdealBandpassFilter(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth);
float * filter_fir_bandpass(FilterBlock_t * self, float * buf, uint32_t buffer_size);
