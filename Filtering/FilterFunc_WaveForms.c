#include "FilterFunc_WaveForms.h"
#include "FilterFunc_FFT.h"

//static float * filter_constant_sine_wave(FilterBlock_t * self, float * buf, uint32_t buffer_size){
//  static uint32_t stateindex = 0;
//  for(uint32_t i=0;i<buffer_size;i++){
//    buf[i] = 8000.0*arm_sin_f32(2*3.14*stateindex/36.0);  //440hz at 16khz
//    stateindex++;
//  }
//
//  return buf;
//}

//
//static float * filter_constant_square_wave(FilterBlock_t * self, float * buf, uint32_t buffer_size){
//  for(uint32_t i=0;i<buffer_size;i++){
//    if(i%24 > 12)
//      buf[i] = 10000.0;
//    else
//      buf[i] = 0.0;
//  }
//
//  return buf;
//}
//
float * filter_sawtooth(FilterBlock_t * self, float * buf, uint32_t buffer_size){

  FilterBlock_LogDebug(self, "SAW");
  int freq_value = FilterFunc_FFT_GetMaxFrequencyValue();
  int freq_index = FilterFunc_FFT_GetMaxFrequencyIndex();

  // sampling frequency / fft size = bin size
  //int freq_hz = freq_index * 4;


  //if fft magnitude less than threshold, output zero
  //otherwise calculate sawtooth formula

  //if freq_index = 100 (400Hz)
  //

  static int last_index =0;
  static int last_freq_index = 0;

  for(uint32_t i=0; i<buffer_size; i++){

    //limit the freq_value
    if(freq_value > 2000){
      freq_value = 2000;
    }

    //keep the phase the same
    if(last_freq_index != freq_index){
      last_index = last_freq_index * last_index / freq_index;
      last_freq_index = freq_index;
    }


    //buf[i] = freq_value * arm_sin_f32(  4.0 *  2.0*PI* (float)freq_index * (float)last_index / 16000.0); //multiply by bin size? 4?

    buf[i] = 2* freq_value * ( last_index * freq_index / 16000.0 ) - (freq_value/2);

    buf[i] = buf[i] * 8; //put some of the energy back

    last_index++;

    //reset sawtooth
    if(last_index * freq_index * 2 >= 16000.0)
      last_index = 0;
  }

  return buf;
}
