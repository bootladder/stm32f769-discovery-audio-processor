#include "FilterTreeNode.h"
#include "FilterBlock.h"

// Defined externally by user.
#define NUMBER_OF_FILTER_BLOCKS 20
static FilterBlock_t * ArrayOfFilterBlocks;

static float AllOfTheScratchBuffers[20][AUDIO_BUFFER_SIZE];
static uint8_t scratchBufferIndex = 0;

static float * _ScratchBuffer_New(void);

void FilterTreeNode_SetPointerToFilterBlockArray(FilterBlock_t * blocks)
{
  ArrayOfFilterBlocks = blocks;
}

float * FilterTreeNode_Execute(FilterTreeNode_t * tree, float * input_buf, uint32_t buffer_size){

  FilterBlock_t * filter = &ArrayOfFilterBlocks[tree->filter_block_index];
  float * output_buf = FilterBlock_ApplyFilter(filter, input_buf, buffer_size);

  if(tree->num_childs == 0)  //terminate recursion
    return output_buf;

  //Accumulator for adding up all the child trees
  float * accumulator = _ScratchBuffer_New();
  for(uint32_t i=0; i<buffer_size; i++){
    accumulator[i] = 0.0;
  }

  //Recurse the tree and add up all childs
  for(int i=0; i<tree->num_childs; i++){

    //Make a new copy of the buffer for each child
    float * output_buf_copy = _ScratchBuffer_New();
    for(uint32_t i=0; i<buffer_size; i++){
      output_buf_copy[i] = output_buf[i];
    }

    float * child_buf = FilterTreeNode_Execute(tree->childs[i], output_buf_copy, buffer_size);

    for(uint32_t i=0;i<buffer_size;i++){
      accumulator[i] += child_buf[i];
    }
  }
  return accumulator;
}


static float * _ScratchBuffer_New(void){
  scratchBufferIndex++;
  if(scratchBufferIndex >= 20){
    scratchBufferIndex = 0;
  }
  return AllOfTheScratchBuffers[scratchBufferIndex];
}

