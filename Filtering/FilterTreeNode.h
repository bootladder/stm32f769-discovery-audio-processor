#ifndef __FILTERTREENODE_H_
#define __FILTERTREENODE_H_

#include <stdint.h>
#include "FilterBlock.h"

typedef struct node {
  uint8_t filter_block_index;
  uint8_t num_childs;
  struct node * childs[5];
} FilterTreeNode_t;

float * FilterTreeNode_Execute(FilterTreeNode_t * tree, float * input_buf, uint32_t buffer_size);

void FilterTreeNode_SetPointerToFilterBlockArray(FilterBlock_t * blocks);


#endif
