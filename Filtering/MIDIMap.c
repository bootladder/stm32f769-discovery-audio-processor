#include "MIDIMap.h"
#include "logger.h"
#include "tinyprintf.h"

MIDIMapEntry_t map[20];
static int map_size = 0;


static bool _match_message(MIDI_Message_t * message1, MIDI_Message_t * message2);


//does not handle duplicates
void MIDIMap_Add(FilterBlock_t * self, MIDIMapEntry_t * entry)
{
  map[map_size].message = entry -> message;
  map[map_size].callback = entry -> callback;
  map[map_size].self = self;
  map_size++;
}

bool MIDIMap_CheckForMatch(MIDI_Message_t message)
{
  //scan the map, look for a match
  for(int i=0; i<map_size; i++){
    if(true == _match_message(&(map[i].message), &message))
      return true;
  }
  return false;
}

static bool _match_message(MIDI_Message_t * message1, MIDI_Message_t * message2)
{
  if(message1 -> status != message2 -> status)
    return false;
  if(message1 -> id != message2 -> id)
    return false;
  //if(message1 -> value != message2 -> value)
  //  return false;

  return true;
}


//scan the map, look for a match,
//call the callback
void MIDIMap_HandleMessage(MIDI_Message_t message)
{
  for(int i=0; i<map_size; i++){
    if(true == _match_message(&(map[i].message), &message))
      {
        Logger_Debug("\n\nMatch!!!!\n");

        static char str[100];
        tfp_snprintf(str, 100, "callback: %p , self : %p , val : %d", map[i].callback, map[i].self, message.value);
        Logger_Debug(str); 

        map[i].callback(map[i].self, message.value);
        return;
      }
  }

  Logger_Debug("No Match\n");

  ASSERT(map_size != 0);
}
