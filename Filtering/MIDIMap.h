#ifndef __MIDIMAP_H_
#define __MIDIMAP_H_

#include <stdbool.h>
#include "MIDIMessage.h"


//figure out circular depennecy
struct block;
typedef struct block FilterBlock_t;
typedef void (*FilterBlockMIDICallback_t)(FilterBlock_t * self, uint8_t value);

typedef struct {
  MIDI_Message_t message;
  FilterBlockMIDICallback_t callback;
  FilterBlock_t * self;
}MIDIMapEntry_t;

void MIDIMap_Add(FilterBlock_t * self, MIDIMapEntry_t * entry);
bool MIDIMap_CheckForMatch(MIDI_Message_t message);
void MIDIMap_HandleMessage(MIDI_Message_t message);

#endif

