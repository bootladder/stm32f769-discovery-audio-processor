#ifndef __MIDIMESSAGE_H_
#define __MIDIMESSAGE_H_

#include <stdint.h>

typedef struct {
  uint8_t status;
  uint8_t id;
  uint8_t value;
}MIDI_Message_t;

#endif
