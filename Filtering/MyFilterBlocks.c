#include "MyFilterBlocks.h"

#include "FilterBlock.h"
#include "stm32f769xx.h"
#include "arm_math.h"

#include "tinyprintf.h"
#include "logger.h"
#include "helpers.h"

#include "steve_rtos.h"
#include "ContiguousBlockStorage.h"

#include "FilterFunc_FFT.h"
#include "FilterFunc_FIR.h"
#include "FilterFunc_WaveForms.h"


static float * filter_one_half_volume(FilterBlock_t * self, float * buf, uint32_t buffer_size){
  FilterBlock_LogDebug(self, "HALF");

  for(uint32_t i=0;i<buffer_size;i++)
    buf[i] = buf[i]/2.0;


  return buf;
}



static float * filter_test(FilterBlock_t * self, float * buf, uint32_t buffer_size){


  //delay line.  add a delayed version to itself
  // 256 sample buffer at 16khz = 16ms.
  //try just keeping the last buffer
#define LAST_BUFFER_SIZE 256
  static float last_buffer_1[LAST_BUFFER_SIZE];
  static float last_buffer_2[LAST_BUFFER_SIZE];
  static float last_buffer_3[LAST_BUFFER_SIZE];
  static float last_buffer_4[LAST_BUFFER_SIZE];
  static float last_buffer_5[LAST_BUFFER_SIZE];
  static float last_buffer_6[LAST_BUFFER_SIZE];


  //add the last buffer to this and divide by 2
  for(int i=0;i<LAST_BUFFER_SIZE;i++){
    buf[i] = (buf[i] +  last_buffer_6[i]) / 2;
  }


  //update the last buffers
  for(int i=0;i<LAST_BUFFER_SIZE;i++){
    last_buffer_6[i] = last_buffer_5[i];
    last_buffer_5[i] = last_buffer_4[i];
    last_buffer_4[i] = last_buffer_3[i];
    last_buffer_3[i] = last_buffer_2[i];
    last_buffer_2[i] = last_buffer_1[i];
    last_buffer_1[i] = buf[i];
  }



  return buf;
}

static float * filter_test_second(FilterBlock_t * self, float * buf, uint32_t buffer_size){
  return buf;
}



static float * filter_diagnostic(FilterBlock_t * self, float * buf, uint32_t buffer_size){

  //count zero crossings
  int num_zero_crossings = 0;
  int zero_crossings[100];
  int zeroes_index = 0 ;

  int state = -1;

  for(uint32_t i=0; i< buffer_size; i++){
    switch(state){

    case -1:
      if(buf[i] > 1000.1){
        state = 1;
        zero_crossings[zeroes_index++] = i;
        num_zero_crossings++;
      }
      break;

    case 1:
      if(buf[i] < -1000.1){
        state = -1;
        zero_crossings[zeroes_index++] = i;
        num_zero_crossings++;
      }
    break;
    }
  }


  static char str[20];
  tfp_snprintf(str, 20, "\nZeroes: %d\n", num_zero_crossings);
  Logger_Debug(str);
  Logger_Debug("Distances Between:  ");

  for(int i=1; i<zeroes_index; i++){
    int distance = zero_crossings[i] - zero_crossings[i-1];
    tfp_snprintf(str, 20, "%08d  ", distance);
    Logger_Debug(str);
  }



  //track peak
  float peak = 0.0;
  for(uint32_t i=0; i< buffer_size; i++){
    if(buf[i] > peak)
      peak = buf[i];
  }


  static char str2[20];
  static char floatstr[20];
  tfp_snprintf(str2, 20, "Peak: %s\n", float2string(peak,floatstr ));
  Logger_Debug(str2);

  return buf;
}


static FilterBlock_t AllOfTheFilterBlocks[MAX_NUMBER_OF_FILTER_BLOCKS] =
  {
    {
      .filter = filter_one_half_volume,
      .enabled = 0,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 20,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 20,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 2,
    },

    /////////////////////////////////////////////////////////

    {
      .filter = filter_diagnostic,
      .enabled = 0,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 21,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 21,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 3,
    },

    /////////////////////////////////////////////////////////

    {
      .filter = filter_fir_bandpass,
      .enabled = 0,
      .cutoff_freq = 200,
      .bandwidth = 100,
      .numFilterTaps = 200,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 22,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 22,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 4,
    },


    /////////////////////////////////////////////////////////

    {
      .filter = filter_sawtooth,
      .enabled = 0,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 23,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 23,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 5,
    },

    /////////////////////////////////////////////////////////

    {
      .filter = filter_test,
      .enabled = 1,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 24,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 24,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 6,
    },

    /////////////////////////////////////////////////////////

    {
      .filter = filter_test_second,
      .enabled = 1,
      .midiMapEntries = {
        {
          .message = {
            .status = 0x90,
            .id = 25,
          },
          .callback = FilterBlock_Enable,
        },
        {
          .message = {
            .status = 0x80,
            .id = 25,
          },
          .callback = FilterBlock_Disable,
        },
      },
      .logging_id = 7,
    },

    /////////////////////////////////////////////////////////
  };

static FilterBlock_t * _my_filter_blocks = AllOfTheFilterBlocks;

FilterBlock_t * MyFilterBlocks_Get(uint8_t index)
{
  return &_my_filter_blocks[index];
}


// for testing purposes
void MyFilterBlocks_SetForTestingPurposes(FilterBlock_t * newBlocks)
{
  _my_filter_blocks = newBlocks;
}


