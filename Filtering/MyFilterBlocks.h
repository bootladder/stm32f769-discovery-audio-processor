#include "FilterBlock.h"

#define MAX_NUMBER_OF_FILTER_BLOCKS 20

FilterBlock_t * MyFilterBlocks_Get(uint8_t index);

void MyFilterBlocks_SetForTestingPurposes(FilterBlock_t * newBlocks);
float * MyFilterBlocks_ComputeCoefficients_IdealBandpassFilter(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth);


enum ENUMERATED_FILTERBLOCK_IDS {
  FILTER_ONE_HALF_VOLUME,
  FILTER_DIAGNOSTIC,
  FILTER_BANDPASS,
  FILTER_SAWTOOTH,
  FILTER_TEST,
  FILTER_TEST_SECOND,
};
