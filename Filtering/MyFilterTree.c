#include "FilterTreeNode.h"
#include "MyFilterBlocks.h"

//this is extern (used externally)
FilterTreeNode_t MyFilterTree[100] = {
  {
    //index = 0
    .filter_block_index = FILTER_DIAGNOSTIC,
    .childs = {
      &MyFilterTree[1],
      0,
      0,
      0,
      0
    },
    .num_childs = 1,
  },
  {
    //index = 1
    .filter_block_index = FILTER_ONE_HALF_VOLUME,
    .childs = {
      &MyFilterTree[2],
      0, //&MyFilterTree[2],
      0,
      0,
      0,
    },
    .num_childs = 1,
  },
  {
    //index = 2
    .filter_block_index = FILTER_SAWTOOTH,
    .childs = {
      &MyFilterTree[3],
      0,
      0,
      0,
      0,
    },
    .num_childs = 1,
  },
  {
    //index = 3
    .filter_block_index = FILTER_BANDPASS,
    .childs = {
      &MyFilterTree[4],
      0,
      0,
      0,
      0},
    .num_childs = 1,
  },
  {
    //index = 4
    .filter_block_index = FILTER_TEST,
    .childs = {0,0,0,0,0},
    .num_childs = 0,
  }
};
Hangouts that want to ask questions and to be able to s
