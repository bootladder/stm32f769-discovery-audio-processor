#include "helpers.h"

#include "tinyprintf.h"
#include "stm32f769xx.h"


// uses tfp_snprintf
char * float2string(float value,char * str)
{
  //zero it out
  for(int i=0;i<20;i++)
    str[i] = '\0';

  ////printf("\n\n");
  //if (isnan(value)){
  //  str[0] = 'n'; str[1] = 'a'; str[2] = 'n';str[3] = '\0';
  //  return str;
  //};


  //if (isinf(value)){
  //  str[0] = 'i'; str[1] = 'n'; str[2] = 'f'; str[3] = '\0';
  //  return str;
  //}

  int blah = (int) value;
  //printf("\n\n%d\n\n", blah);

  int numchars = tfp_snprintf(str, 100, "%d", blah);
  str[numchars] = '.';

  //printf("\n\n%s\n\n", str);
  float floatingpart = value - (float)blah;

  //take 5 decimals
  int pos = numchars + 1;
  int i=0;
  for(;i<6;i++){
    floatingpart *= 10;
    //printf("floating part %f\n", floatingpart);
    str[pos+i] = '0' + (int)floatingpart;
    //printf("int of floating part %d\n", (int)floatingpart);
    //printf("str %s\n", str);
    int intpart = (int)floatingpart;
    floatingpart = floatingpart - (float)intpart;
    //printf("2nd floating part %f\n", floatingpart);
  }

  str[numchars+i] = '\0';

  //printf("\n\n%s\n\n", str);
  return str;

}



//returns length of string
int STM32F7_Help_GetAllEnableIRQ(char * buf)
{
  //MDIOS_IRQn = 109

  char * fmt = "IRQn %03d : %d\n";
  char * buf_trav = buf;
  int numwritten = 0;

  for(int i=0; i<109; i++){

    if(NVIC_GetEnableIRQ(i))
      numwritten = tfp_snprintf(buf_trav, 20, fmt, i, 1);
    else
      numwritten = tfp_snprintf(buf_trav, 20, fmt, i, 0);

    buf_trav = buf_trav + numwritten;
  }

  buf_trav[0] = '\0';

  return buf_trav - buf;
}


//char irqstringbuf[3000];
//for(int i=0;i<3000;i++){
//  irqstringbuf[i] = 'Z';
//}
//int len = STM32F7_Help_GetAllEnableIRQ(irqstringbuf);
//Steve_UART_Transmit(irqstringbuf,len);

