#include "logger.h"
#include <stdarg.h>


//PRODUCTION

#ifndef TEST_BUILD

#include "steve_uart_debug.h"
#include "tinyprintf.h"
#include "helpers.h"

void Logger_Debug(char * str)
{
  Steve_UART_DEBUG_Transmit_String(str);
}

void Logger_Debug_Int(int i)
{
  static char intbuf[20];
  tfp_snprintf(intbuf,20, "%08d", i);
  Steve_UART_DEBUG_Transmit_String(intbuf);
}

void Logger_Debug_IntHex(int i)
{
  static char intbuf[20];
  tfp_snprintf(intbuf,20, "%08X", i);
  Steve_UART_DEBUG_Transmit_String(intbuf);
}

void Logger_Debug_Float(float f)
{
  static char floatbuf[20];
  Steve_UART_DEBUG_Transmit_String(float2string(f, floatbuf));
}

void Logger_Printf(char * fmt, ...)
{
  ; //figure it out
}





#else
#include <stdio.h>

//TEST 

#define SILENCE
void Logger_Debug(char * str)
{
#ifndef SILENCE
  puts(str);
#endif
  (void)str;
}

void Logger_Printf(char * fmt, ...)
{
  #ifndef SILENCE
  va_list va;
  va_start (va, fmt);
  vprintf(fmt, va);
  va_end(va);
  #endif
  (void)fmt;
}

void Logger_Debug_Int(int i){
  
  (void)i;
}

void Logger_Debug_IntHex(int i){
  
}
#endif

