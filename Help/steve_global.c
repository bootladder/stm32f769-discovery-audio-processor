#include "steve_global.h"
#include "steve_uart.h"
#include "logger.h"
#include "stm32f769i_discovery.h"
#include "tinyprintf.h"

static char breadcrumb_string[128] = "hurr";
char * Steve_GLOBAL_GetBreadcrumbString(void)
{
  return breadcrumb_string;
}


//must be a zero terminated string
void Steve_GLOBAL_CopyBreadcrumbString(char * str){
  
  for(int i=0; i<128;i++){
    if (str[i] == 0){
      return;
    }
    breadcrumb_string[i] = str[i];
  }
}


void Steve_GLOBAL_InfiniteLoop(){
  Logger_Debug("I am the infinite loop        ");
  for( ;; ){
    BSP_LED_Toggle(LED1);
    for(int i=0;i<100000;i++){
      for(int z=0; z<30; z++){
        Logger_Debug("z");
      }
      for(int x=0; x<30; x++){
        Logger_Debug("\b"); //backspace
      }
      BSP_LED_Toggle(LED2);
    }
  }
}

void Steve_GLOBAL_InfiniteLoop_WithString(char * fileplace){
  Logger_Debug("\n\n\n");
  Logger_Debug(fileplace);
  Logger_Debug("\nI am the infinite loop (withstring): ");
  BSP_LED_Off(LED2);
  for( ;; ){
    BSP_LED_Toggle(LED1);
    for(int i=0;i<30;i++){
      for(int z=0; z<30; z++){
        Logger_Debug("z");
      }
      for(int x=0; x<30; x++){
        Logger_Debug("\b"); //backspace
      }
    }
  }
}


void Steve_GLOBAL_PrintString(char * str){
  Logger_Debug(str);
}
void Steve_GLOBAL_PrintNumber(int number){
  static char numbuf[10];
  tfp_snprintf(numbuf,10,"%d", number);
  Logger_Debug(numbuf);
}
void Steve_GLOBAL_PrintStringAndNumber(char * str,int number){
  Steve_GLOBAL_PrintString(str);
  Steve_GLOBAL_PrintNumber(number);
}

void STEVE_GLOBAL_FORBIDDEN_SECTION(void){

  Steve_GLOBAL_InfiniteLoop_WithString("Forbidden Section Reached");
}


void Steve_GLOBAL_LED_OFF(void){
  BSP_LED_Off(LED1);
}
void Steve_GLOBAL_LED_ON(void){
  BSP_LED_On(LED1);
}
void Steve_GLOBAL_LED_TOGGLE(void){
  static int i=0;
  if(i==0){
    BSP_LED_On(LED1);
    i = 1;
  }
  else{
    BSP_LED_Off(LED1);
    i = 0;
  }
}

void ASSERT(bool cond){
  if(false == cond){
    //__disable_irq();
    Steve_GLOBAL_InfiniteLoop_WithString("assert failed");
  }
}
