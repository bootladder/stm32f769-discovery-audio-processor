#include <stdint.h>
#include <stdbool.h>

char * Steve_GLOBAL_GetBreadcrumbString(void);


void Steve_GLOBAL_InfiniteLoop();
void Steve_GLOBAL_InfiniteLoop_WithString(char * fileplace);

void Steve_GLOBAL_PrintString(char * str);
void Steve_GLOBAL_PrintNumber(int number);
void Steve_GLOBAL_PrintStringAndNumber(char * str,int number);

void STEVE_GLOBAL_FORBIDDEN_SECTION(void);

void Steve_GLOBAL_LED_TOGGLE(void);

void Steve_GLOBAL_Assert(bool cond);
void ASSERT(bool cond);
