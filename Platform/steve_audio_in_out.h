
uint8_t Steve_AUDIO_IN_OUT_Init(uint32_t sample_freq, uint32_t buf_size);
uint8_t Steve_AUDIO_IN_OUT_CheckBuffers(void);
void Steve_AUDIO_IN_OUT_CopyBufferCallback(int16_t *pbuffer1, int16_t *pbuffer2, uint16_t BufferSize);
