#include "steve_lcd_logger.h"
#include <stdint.h>
#include "steve_rtos.h"
#include "tinyprintf.h"
#include "stm32f769i_discovery.h"
#include "stm32f769i_discovery_lcd.h"

#define STRING_LENGTH 40
#define TABLE_LENGTH 20

static char tableOfStrings[STRING_LENGTH][TABLE_LENGTH] = {
  "hello1",
  "hello1",
  "hello1",
  "hello1",
  "hello1",
  "hello1",
  "hello1",
  "hello1",
  "hello1",
};

static void _Display_LCD(char * str, uint8_t position){
  BSP_LCD_DisplayStringAt(0, 20*position, (uint8_t *)str, CENTER_MODE);
}

static void _Clear_LCD(void){
  BSP_LCD_Clear(LCD_COLOR_WHITE);
}

void Steve_LCD_Logger_DisplayStringAtLine(char * str, int lineNum)
{
  tfp_snprintf(tableOfStrings[lineNum], STRING_LENGTH, "%s", str);
}


void LCD_Thread(void const * argument)
{
  while(1){
    uint32_t ticks = Steve_RTOS_GetTotalTickCount(); //includes time while interrupted by other tasks
    _Clear_LCD();

    for(int i=0; i<20; i++){
      _Display_LCD(tableOfStrings[i], i);
    }

    ticks = Steve_RTOS_GetTotalTickCount() - ticks;
    static char numbuf[10];
    tfp_snprintf(numbuf,10,"%ld", ticks);
    _Display_LCD(numbuf, 20);

    vTaskDelay(1000);
  }
}


