#include "steve_midi_input.h"
#include "steve_uart.h"
#include "steve_global.h"
#include "logger.h"
#include "MIDIMessage.h"
#include "MIDIMap.h"
#include <stdbool.h>
#include "tinyprintf.h"

static MIDI_Message_t message;
static bool _midi_message_ready = false;

void Steve_MIDI_INPUT_Task(void)
{
  if(false == MIDIParser_IsNewMessageAvailable())
    return;

  MIDI_Message_t midi_message = MIDIParser_GetMessage();

  char str[20];
  tfp_snprintf(str, 20, "MIDI %02X %02X %02X\n",
               midi_message.status, midi_message.id, midi_message.value);
  Logger_Debug(str);

  Steve_GLOBAL_LED_TOGGLE();

  MIDIMap_HandleMessage(midi_message);
}

// XXXXXABCXXXXXXABCXXXXXQWEXXXXXX
void MIDIParser_ByteReceived(uint8_t byte)
{
  static int state = 0;
  static uint8_t message_bytes[3];

  if(byte == 'X')
    return;

  message_bytes[state] = byte;
  state++;

  if(state >= 3){
    state = 0;
    message.status = message_bytes[0];
    message.id = message_bytes[1];
    message.value = message_bytes[2];
    _midi_message_ready = true;
  }
}

bool MIDIParser_IsNewMessageAvailable(void){
  return _midi_message_ready;
}

MIDI_Message_t MIDIParser_GetMessage(void){
  _midi_message_ready = false;
  return message;
}
