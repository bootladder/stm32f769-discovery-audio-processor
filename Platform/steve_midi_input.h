#ifndef __STEVE_USERINPUT_H__
#define __STEVE_USERINPUT_H__

#include "MIDIMessage.h"

#include <stdbool.h>

void Steve_MIDI_INPUT_Task(void);

//non-static for testing
MIDI_Message_t MIDIParser_GetMessage(void);
void MIDIParser_ByteReceived(uint8_t byte);
bool MIDIParser_IsNewMessageAvailable(void);


#endif
