#include "steve_rtos.h"
#include "steve_global.h"
#include <stdint.h>
#include "FilteringTask.h"
#include "steve_midi_input.h"
#include "steve_lcd_logger.h"

xQueueHandle xQueue;
xSemaphoreHandle xAudioBufferSemaphore;


static osThreadId     AudioThreadId = 0;
static osThreadId     MIDIInputThreadId = 0;
static osThreadId     LCDThreadId = 0;


static void MIDI_Input_Thread(void const * argument);

void Steve_RTOS_Init(void)
{
  xQueue = xQueueCreate( 10, 100 ); //space for 10 items, each of size 100
  if( xQueue == NULL ){
    Steve_GLOBAL_PrintString("queue failed to create\n");
  }

  vSemaphoreCreateBinary( xAudioBufferSemaphore );

  //FilteringTask waits for semaphore, so is highest priority
  int filteringTaskPriority = 10;
  osThreadDef(osFilteringTask, FilteringTask, filteringTaskPriority, 0, 2048);
  AudioThreadId = osThreadCreate (osThread(osFilteringTask), NULL);

  int MIDIInputTaskPriority = 2;
  osThreadDef(osMIDI_Input_Thread, MIDI_Input_Thread, MIDIInputTaskPriority, 0, 2048);
  MIDIInputThreadId = osThreadCreate (osThread(osMIDI_Input_Thread), NULL);

  int LCDTaskPriority = 2;
  osThreadDef(osLCD_Thread, LCD_Thread, LCDTaskPriority, 0, 2048);
  LCDThreadId = osThreadCreate (osThread(osLCD_Thread), NULL);
}


static void MIDI_Input_Thread(void const * argument)
{
  while(1){
    Steve_MIDI_INPUT_Task();
  }
}


    //    portBASE_TYPE xStatus;
    //    const portTickType xTicksToWait = 1000/portTICK_RATE_MS;
    //    static uint8_t receivedData[100];

    //xStatus = xQueueReceive( xQueue, receivedData, xTicksToWait );
    //if( xStatus == pdPASS )
    //  {
    //    Steve_GLOBAL_PrintString( "R");
    //  }
    //else
    //  {
    //    //Steve_UART_Transmit_String( "Could not receive from the queue.\n" );
    //  }

    //vTaskDelay( 100 / portTICK_RATE_MS );


uint16_t osGetCPUUsage (void);
#define CALCULATION_PERIOD    1000

xTaskHandle    xIdleHandle = NULL;
volatile uint32_t  osCPU_Usage = 0; 
uint32_t       osCPU_IdleStartTime = 0; 
uint32_t       osCPU_IdleSpentTime = 0; 
uint32_t       osCPU_TotalIdleTime = 0; 

uint32_t totalTickCount = 0;


uint32_t Steve_RTOS_GetTotalTickCount(void){
  return totalTickCount;
}


void vApplicationTickHook (void)
{

  totalTickCount++;

  static int tick = 0;
  
  if(tick ++ > CALCULATION_PERIOD)
    {
      tick = 0;
    
      if(osCPU_TotalIdleTime > 1000)
        {
          osCPU_TotalIdleTime = 1000;
        }
      osCPU_Usage = (100 - (osCPU_TotalIdleTime * 100) / CALCULATION_PERIOD);
      osCPU_TotalIdleTime = 0;
    }
}


/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Application Idle Hook
  * @param  None 
  * @retval None
  */
void vApplicationIdleHook(void) 
{
  if( xIdleHandle == NULL )
  {
    /* Store the handle to the idle task. */
    xIdleHandle = xTaskGetCurrentTaskHandle();
  }
}

/**
  * @brief  Start Idle monitor
  * @param  None 
  * @retval None
  */
void StartIdleMonitor (void)
{
  if( xTaskGetCurrentTaskHandle() == xIdleHandle ) 
  {
    osCPU_IdleStartTime = xTaskGetTickCountFromISR();
  }
}

/**
  * @brief  Stop Idle monitor
  * @param  None 
  * @retval None
  */
void EndIdleMonitor (void)
{
  if( xTaskGetCurrentTaskHandle() == xIdleHandle )
  {
    /* Store the handle to the idle task. */
    osCPU_IdleSpentTime = xTaskGetTickCountFromISR() - osCPU_IdleStartTime;
    osCPU_TotalIdleTime += osCPU_IdleSpentTime; 
  }
}

/**
  * @brief  Stop Idle monitor
  * @param  None 
  * @retval None
  */
uint16_t osGetCPUUsage (void)
{
  return (uint16_t)osCPU_Usage;
}


/************************ (c) copyright stmicroelectronics *****end of file****/
