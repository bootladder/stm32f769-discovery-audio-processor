
#include "cmsis_os.h"
void Steve_RTOS_Init(void);

extern xQueueHandle xQueue;
extern xSemaphoreHandle xAudioBufferSemaphore;

uint32_t Steve_RTOS_GetTotalTickCount(void);
