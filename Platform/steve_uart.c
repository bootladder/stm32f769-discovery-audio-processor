#include "steve_uart.h"

#include "steve_midi_input.h"

#include "stm32f7xx_hal.h"
#include <stdbool.h>

static HAL_StatusTypeDef init_status = STEVE_UART_INIT_STATUS_NOTHING;

// Extern used in xxx_it.c
UART_HandleTypeDef huart = {};


STEVE_UART_INIT_STATUS_t Steve_UART_GetInitStatus(){ return init_status; }

const char * Steve_UART_GetInitStatusString(){
  switch(init_status){
     case HAL_OK      :
       return "OK!";
     case HAL_ERROR   :
       return "ERROR!";
     case HAL_BUSY    :
       return "BUSY!";
     case HAL_TIMEOUT :
    return "TIMEOUT!";
  }
  return "wat";
}

void Steve_UART_Init(void)
{

  //init
  huart.Instance = USART6;
  huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;

  //config

  huart.Init.BaudRate = 9600;
  huart.Init.WordLength = UART_WORDLENGTH_8B;
  huart.Init.StopBits = UART_STOPBITS_1;
  huart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

  huart.Init.Parity = UART_PARITY_NONE;
  huart.Init.Mode = UART_MODE_TX_RX;
  //oversampling skipped

  init_status = HAL_UART_Init(&huart);

  //HAL_UART_Transmit(&huart, (uint8_t *)"What's up?\n", 11, 1000);
  //HAL_UART_Transmit(&huart, (uint8_t *)"HURRRRRRR?\n", 11, 1000);
  while(HAL_OK != HAL_UART_Transmit(&huart, (uint8_t *)"What's up?\n", 11, 1000))
    ;
  while(HAL_OK != HAL_UART_Transmit(&huart, (uint8_t *)"HURRRRRRR?\n", 11, 1000))
    ;
}


void Steve_UART_Transmit(uint8_t * buf, uint16_t size){
  HAL_UART_Transmit(&huart, buf, size, 1000000);
}

void Steve_UART_Transmit_String(char *str){
  static uint8_t stringbuf[128];
  int i = 0;
  for(;i<128;i++){
    if (str[i] == 0) {
      break;
    }
    stringbuf[i] = str[i];
  }
  HAL_UART_Transmit(&huart, stringbuf, i, 1000);
}


static uint8_t myreceivebuf[1];
static uint8_t received_byte;
static bool receive_buffer_ready = false;

uint8_t Steve_UART_Receive_Bytes(uint8_t length){

  HAL_StatusTypeDef status =
    HAL_UART_Receive_IT(&huart, myreceivebuf, length);
  return status;
}


//implicit for now

//huart is a parameter pointer here
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  received_byte = myreceivebuf[0];
  receive_buffer_ready = true;

  MIDIParser_ByteReceived(received_byte);
  HAL_UART_Receive_IT(huart, myreceivebuf, 1);
}

bool Steve_UART_ReceivedByteReady(void){
  return receive_buffer_ready;
}

uint8_t Steve_UART_PopReceivedByte(void){
  receive_buffer_ready = false;
  return received_byte;
}

////////////////////////////////////////////////////////////
//   MSP
///////////

#define USARTx                           USART6
#define USARTx_CLK_ENABLE()              __USART6_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __USART6_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __USART6_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_6
#define USARTx_TX_GPIO_PORT              GPIOC
#define USARTx_TX_AF                     GPIO_AF8_USART6
#define USARTx_RX_PIN                    GPIO_PIN_7
#define USARTx_RX_GPIO_PORT              GPIOC
#define USARTx_RX_AF                     GPIO_AF8_USART6


/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART6_IRQn
#define USARTx_IRQHandler                USART6_IRQHandler


////////////////////////////////////////////
/**
  * @brief UART MSP Initialization 
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USARTx_TX_GPIO_CLK_ENABLE();
  USARTx_RX_GPIO_CLK_ENABLE();


  /* Enable USARTx clock */
  USARTx_CLK_ENABLE();

  /* Enable DMA clock */
  //DMAx_CLK_ENABLE();
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USARTx_TX_AF;

  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  GPIO_InitStruct.Alternate = USARTx_RX_AF;

  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);

  /* NVIC for USART, to catch the TX complete */
  HAL_NVIC_SetPriority(USARTx_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USARTx_IRQn);


}
/**
  * @brief UART MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{

  /*##-1- Reset peripherals ##################################################*/
  USARTx_FORCE_RESET();
  USARTx_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks #################################*/
  /* Configure USARTx Tx as alternate function  */
  HAL_GPIO_DeInit(USARTx_TX_GPIO_PORT, USARTx_TX_PIN);
  /* Configure USARTx Rx as alternate function  */
  HAL_GPIO_DeInit(USARTx_RX_GPIO_PORT, USARTx_RX_PIN);
   
  // /*##-3- Disable the DMA #####################################################*/
  // /* De-Initialize the DMA channel associated to reception process */
  // if(huart->hdmarx != 0)
  // {
  //   HAL_DMA_DeInit(huart->hdmarx);
  // }
  // /* De-Initialize the DMA channel associated to transmission process */
    // if(huart->hdmatx != 0)
  // {
  //   HAL_DMA_DeInit(huart->hdmatx);
  // }  
  // 
  // /*##-4- Disable the NVIC for DMA ###########################################*/
    // HAL_NVIC_DisableIRQ(USARTx_DMA_TX_IRQn);
  // HAL_NVIC_DisableIRQ(USARTx_DMA_RX_IRQn);
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
