#include <stdint.h>

typedef enum {
  STEVE_UART_INIT_STATUS_NOTHING
}STEVE_UART_INIT_STATUS_t ;

STEVE_UART_INIT_STATUS_t Steve_UART_GetInitStatus();
const char * Steve_UART_GetInitStatusString();

void Steve_UART_Init(void);
void Steve_UART_Transmit(uint8_t * buf, uint16_t size);
void Steve_UART_Transmit_String(char *str);

uint8_t Steve_UART_Receive_Bytes(uint8_t length);
