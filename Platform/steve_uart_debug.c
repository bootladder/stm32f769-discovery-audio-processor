#include "steve_uart.h"

#include "stm32f7xx_hal.h"
#include <stdbool.h>

// Extern used in xxx_it.c
static UART_HandleTypeDef huart = {};


void debug_init_second_uart(void);

void Steve_UART_DEBUG_Init(void)
{

  //init
  huart.Instance = UART5;
  huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;

  //config

  huart.Init.BaudRate = 921600;
  huart.Init.WordLength = UART_WORDLENGTH_8B;
  huart.Init.StopBits = UART_STOPBITS_1;
  huart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

  huart.Init.Parity = UART_PARITY_NONE;
  huart.Init.Mode = UART_MODE_TX_RX;
  //oversampling skipped

  debug_init_second_uart();
  HAL_UART_Init(&huart);

  //HAL_UART_Transmit(&huart, (uint8_t *)"What's up?\n", 11, 1000);
  //HAL_UART_Transmit(&huart, (uint8_t *)"HURRRRRRR?\n", 11, 1000);
  while(HAL_OK != HAL_UART_Transmit(&huart, (uint8_t *)"What's up?\n", 11, 1000))
    ;
  while(HAL_OK != HAL_UART_Transmit(&huart, (uint8_t *)"HURRRRRRR?\n", 11, 1000))
    ;
}


void Steve_UART_DEBUG_Transmit(uint8_t * buf, uint16_t size){
  HAL_UART_Transmit(&huart, buf, size, 1000000);
}

void Steve_UART_DEBUG_Transmit_String(char *str){
  static uint8_t stringbuf[128];
  int i = 0;
  for(;i<128;i++){
    if (str[i] == 0) {
      break;
    }
    stringbuf[i] = str[i];
  }
  HAL_UART_Transmit(&huart, stringbuf, i, 1000);
}


static uint8_t myreceivebuf[1];
static uint8_t received_byte;
static bool receive_buffer_ready = false;

uint8_t Steve_UART_DEBUG_Receive_Bytes(uint8_t length){

  HAL_StatusTypeDef status =
    HAL_UART_Receive_IT(&huart, myreceivebuf, length);
  return status;
}


//implicit for now

//huart is a parameter pointer here
void HAL_UART_DEBUG_RxCpltCallback(UART_HandleTypeDef *huart){
  received_byte = myreceivebuf[0];
  receive_buffer_ready = true;

  //MIDIParser_ByteReceived(received_byte);
  //HAL_UART_Receive_IT(huart, myreceivebuf, 1);
}

bool Steve_UART_DEBUG_ReceivedByteReady(void){
  return receive_buffer_ready;
}

uint8_t Steve_UART_DEBUG_PopReceivedByte(void){
  receive_buffer_ready = false;
  return received_byte;
}


#define USART_Debug                           UART5
#define USART_Debug_CLK_ENABLE()              __USART5_CLK_ENABLE()
#define USART_Debug_RX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()
#define USART_Debug_TX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()

#define USART_Debug_FORCE_RESET()             __USART5_FORCE_RESET()
#define USART_Debug_RELEASE_RESET()           __USART5_RELEASE_RESET()

/* Definition for USART_Debug Pins */
#define USART_Debug_TX_PIN                    GPIO_PIN_12
#define USART_Debug_TX_GPIO_PORT              GPIOC
#define USART_Debug_TX_AF                     GPIO_AF8_UART5
#define USART_Debug_RX_PIN                    GPIO_PIN_12
#define USART_Debug_RX_GPIO_PORT              GPIOD
#define USART_Debug_RX_AF                     GPIO_AF8_UART5


/* Definition for USARTx's NVIC */
#define USART_Debug_IRQn                      UART5_IRQn
#define USART_Debug_IRQHandler                UART5_IRQHandler


void debug_init_second_uart(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USART_Debug_TX_GPIO_CLK_ENABLE();
  USART_Debug_RX_GPIO_CLK_ENABLE();


  /* Enable USARTx clock */
  USART_Debug_CLK_ENABLE();

  /* Enable DMA clock */
  //DMAx_CLK_ENABLE();
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USART_Debug_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USART_Debug_TX_AF;

  HAL_GPIO_Init(USART_Debug_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USART_Debug_RX_PIN;
  GPIO_InitStruct.Alternate = USART_Debug_RX_AF;

  HAL_GPIO_Init(USART_Debug_RX_GPIO_PORT, &GPIO_InitStruct);

  /* NVIC for USART, to catch the TX complete */
  HAL_NVIC_SetPriority(USART_Debug_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USART_Debug_IRQn);

}
