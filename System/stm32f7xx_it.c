#include "stm32f7xx_hal.h"
#include "stm32f769i_discovery_audio.h"

extern HCD_HandleTypeDef hhcd;
/* SAI handler declared in "stm32f769i_discovery_audio.c" file */
extern SAI_HandleTypeDef haudio_out_sai;
extern SAI_HandleTypeDef haudio_in_sai;

extern DFSDM_Filter_HandleTypeDef       hAudioInTopLeftFilter;
extern DFSDM_Filter_HandleTypeDef       hAudioInTopRightFilter;

extern DFSDM_Filter_HandleTypeDef       hAudioInButtomLeftFilter;
extern DFSDM_Filter_HandleTypeDef       hAudioInButtomRightFilter;

extern UART_HandleTypeDef       huart;

void SysTick_Handler(void)
{
  HAL_IncTick();
  osSystickHandler();

}

void DMA2_Stream4_IRQHandler(void)
{
  //Steve_GLOBAL_PrintString("I");
  HAL_DMA_IRQHandler(haudio_in_sai.hdmarx);
}

void AUDIO_OUT_SAIx_DMAx_IRQHandler(void)
{
  //Steve_GLOBAL_PrintString("O");
  HAL_DMA_IRQHandler(haudio_out_sai.hdmatx);
}

void AUDIO_DFSDMx_DMAx_TOP_LEFT_IRQHandler(void)
{
  //  HAL_DMA_IRQHandler(hAudioInTopLeftFilter.hdmaReg);
}

void AUDIO_DFSDMx_DMAx_TOP_RIGHT_IRQHandler(void)
{
  /* HAL_DMA_IRQHandler(hAudioInTopRightFilter.hdmaReg); */
}

void AUDIO_DFSDMx_DMAx_BUTTOM_LEFT_IRQHandler(void)
{
  /* HAL_DMA_IRQHandler(hAudioInButtomLeftFilter.hdmaReg); */
}

void AUDIO_DFSDMx_DMAx_BUTTOM_RIGHT_IRQHandler(void)
{
  /* HAL_DMA_IRQHandler(hAudioInButtomRightFilter.hdmaReg); */
}



void USART6_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart);
}
