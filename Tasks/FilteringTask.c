#include "FilteringTask.h"

#include "steve_rtos.h"
#include "steve_audio_in_out.h"

#include "FilterBlock.h"
#include "MyFilterBlocks.h"

#include "logger.h"
#include "steve_lcd_logger.h"
#include "tinyprintf.h"

#include "ContiguousBlockStorage.h"
#include "FilterFunc_FFT.h"

#include "FilterTreeNode.h"
extern FilterTreeNode_t MyFilterTree;

#include "stm32f769i_discovery_audio.h"
#define SAMPLING_FREQUENCY BSP_AUDIO_FREQUENCY_16K
#define SAMPLING_BUFFER_SIZE 256  //256 samples per interrupt * 2 ping ping * 4 samples per frame = 4k

void FilteringTask(void const * argument)
{
  Steve_AUDIO_IN_OUT_Init(SAMPLING_FREQUENCY, SAMPLING_BUFFER_SIZE);

  // Initialize Filtering System
  FilterBlock_SetPointerToFilterBlockArray(MyFilterBlocks_Get(0));
  FilterTreeNode_SetPointerToFilterBlockArray(MyFilterBlocks_Get(0));

  for(int i=0; i<MAX_NUMBER_OF_FILTER_BLOCKS; i++){
    FilterBlock_RegisterMIDIMap(MyFilterBlocks_Get(i));
  }

  FilterBlock_SetComputeFunction(MyFilterBlocks_Get(2), MyFilterBlocks_ComputeCoefficients_IdealBandpassFilter);
  FilterBlock_ComputeFilterCoefficients(MyFilterBlocks_Get(2));
  Logger_Debug( FilterBlock_CoefficientsToString(MyFilterBlocks_Get(2)));

  Logger_Debug( FilterBlock_ToString(MyFilterBlocks_Get(0)));
  Logger_Debug( FilterBlock_ToString(MyFilterBlocks_Get(3)));


  while(1){
    xSemaphoreTake( xAudioBufferSemaphore, portMAX_DELAY );  //wait for audio buffer to fill
    //Logger_Clear_LCD();
    //Logger_Debug("\033[2J");
    //Logger_Debug("\033[H");
    uint32_t ticks = Steve_RTOS_GetTotalTickCount();

    Steve_AUDIO_IN_OUT_CheckBuffers();

    ticks = Steve_RTOS_GetTotalTickCount() - ticks;
    char str[100];
    tfp_snprintf(str,100,  "\nTick Time??: %lu\n", ticks);
    Steve_LCD_Logger_DisplayStringAtLine(str, 12);
  }
}

// Playback, Record, Size
// Due to SAI frame having 4 bytes for both record and playback buffers,
// And due to this system being MONO only,
// Ignore the last 3 bytes, only take the 1st byte of a frame.
void Steve_AUDIO_IN_OUT_CopyBufferCallback(int16_t *pbuffer1, int16_t *pbuffer2, uint16_t BufferSize)
{
  static float inputFloatBuffer[4096];
  float * outputFloatBuffer;  //memory is allocated within FilterTreeNode
  int actual_mono_signal_buffer_size = BufferSize/4;

  //convert input buffer to floating point
  for(int i=0;i<actual_mono_signal_buffer_size;i++){
    inputFloatBuffer[i] = (float)(pbuffer2[i*4]);
  }

  //Keep rolling FFT
  ContiguousBlockStorage_StoreBlock(inputFloatBuffer);
  FilterFunc_FFT(ContiguousBlockStorage_Beginning(), ContiguousBlockStorage_BufferSize());


  //Apply Filters on new buffer
  outputFloatBuffer = FilterTreeNode_Execute(&MyFilterTree, inputFloatBuffer, BufferSize/4);

  //Convert Output Buffer to signed 16bit integer
  for(int i=0;i<actual_mono_signal_buffer_size;i++){
    pbuffer1[i*4] = (int16_t)(outputFloatBuffer[i]);
  }
}

