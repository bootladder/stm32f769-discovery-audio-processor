#include <stdint.h>
#include "stm32f769i_discovery.h"
#include "stm32f769i_discovery_lcd.h"
#include "tinyprintf.h"
#include "steve_uart.h"
#include "steve_uart_debug.h"
#include "steve_global.h"
#include "steve_rtos.h"
#include "cmsis_os.h"

static void SystemClock_Config(void);


int main(void)
{
  //Configure CPU, HAL, BSP
  HAL_Init();
  SystemClock_Config();///* Configure the system clock @ 200 Mhz */

  Steve_UART_Init();
  Steve_UART_Receive_Bytes(1); //continuously receive bytes
  Steve_UART_DEBUG_Init();
  Steve_UART_DEBUG_Transmit_String("Hello");

  BSP_LED_Init(LED1);
  BSP_LED_Init(LED2);
  BSP_LED_Off(LED1);
  BSP_LED_Off(LED2);

  BSP_LCD_Init();
  BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS);
  BSP_LCD_SelectLayer(1);
  BSP_LCD_DisplayOn();
  BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
  BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
  BSP_LCD_Clear(LCD_COLOR_WHITE);
  BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
  BSP_LCD_DisplayStringAt(0, 10, (uint8_t *)"HURR DURR", CENTER_MODE);
  BSP_LCD_DisplayStringAt(0, 100, (uint8_t *)"Watt, wat", CENTER_MODE);

  // Audio is initialized in Filter Task

  Steve_RTOS_Init(); // Initialize RTOS resources ie. tasks, queues


  osKernelStart();  //pass control to scheduler

  while (1)  {;}
}



void vApplicationMallocFailedHook(xTaskHandle xtask)
{
  Steve_GLOBAL_InfiniteLoop_WithString("malloc failed");
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
  Steve_GLOBAL_InfiniteLoop_WithString("stack overflow");
}
uint8_t  ucHeap[configTOTAL_HEAP_SIZE];




/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 200000000
  *            HCLK(Hz)                       = 200000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 400
  *            PLL_P                          = 2
  *            PLLSAI_N                       = 384
  *            PLLSAI_P                       = 8
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 6
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  ret = HAL_PWREx_EnableOverDrive();

  if(ret != HAL_OK)
  {
    while(1) { ; }
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
}



#ifdef USE_FULL_ASSERT
/**
* @brief  assert_failed
*         Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  File: pointer to the source file name
* @param  Line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line
  number,ex: printf("Wrong parameters value: file %s on line %d\r\n",
  file, line) */

  /* Infinite loop */
  while (1)

  {}
}
#endif



//from freertos port.c
void xPortPendSVHandler( void ) __attribute__ (( naked ));
void vPortSVCHandler( void );


void Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("Default_Handler()");
}
void NMI_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("NMI_Default_Handler()");
}
void MemManage_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("MemManage_Default_Handler()");
}
void BusFault_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("BusFault_Default_Handler()");
}
void UsageFault_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("UsageFault_Default_Handler()");
}
void SVC_Default_Handler(){
  vPortSVCHandler();
  //Steve_GLOBAL_InfiniteLoop_WithString("SVC_Default_Handler()");
}
void DebugMon_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("DebugMon_Default_Handler()");
}
void PendSV_Default_Handler(){
  xPortPendSVHandler();
  //Steve_GLOBAL_InfiniteLoop_WithString("PendSV_Default_Handler()");
}
void Other_Default_Handler(){
  Steve_GLOBAL_InfiniteLoop_WithString("Other_Default_Handler()");
}

void Weak_Handler_HardFault(){
  Steve_GLOBAL_InfiniteLoop_WithString("HARD FAULT Handler");
}
void Weak_Handler_SysTick(){
  Steve_GLOBAL_InfiniteLoop_WithString("Weak SysTick Handler");
}
void Weak_Handler_Other(){
  Steve_GLOBAL_InfiniteLoop_WithString("Other DefaultHandler");
}

//void Weak_DMA1_Stream0_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream0 Handler");}
//void Weak_DMA1_Stream1_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream1 Handler");}
//void Weak_DMA1_Stream2_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream2 Handler");}
//void Weak_DMA1_Stream3_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream3 Handler");}
//void Weak_DMA1_Stream4_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream4 Handler");}
//void Weak_DMA1_Stream5_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream5 Handler");}
//void Weak_DMA1_Stream6_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream6 Handler");}
//void Weak_DMA1_Stream7_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA1 Stream7 Handler");}
//
//void Weak_DMA2_Stream0_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream0 Handler");}
//void Weak_DMA2_Stream1_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream1 Handler");}
//void Weak_DMA2_Stream2_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream2 Handler");}
//void Weak_DMA2_Stream3_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream3 Handler");}
//void Weak_DMA2_Stream4_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream4 Handler");}
//void Weak_DMA2_Stream5_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream5 Handler");}
//void Weak_DMA2_Stream6_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream6 Handler");}
//void Weak_DMA2_Stream7_IRQHandler(){Steve_GLOBAL_InfiniteLoop_WithString("Weak DMA2 Stream7 Handler");}

#define doSomething(a) Steve_GLOBAL_PrintString(a)

void Weak_DMA1_Stream0_IRQHandler(){doSomething("Weak DMA1 Stream0 Handler");}
void Weak_DMA1_Stream1_IRQHandler(){doSomething("Weak DMA1 Stream1 Handler");}
void Weak_DMA1_Stream2_IRQHandler(){doSomething("Weak DMA1 Stream2 Handler");}
void Weak_DMA1_Stream3_IRQHandler(){doSomething("Weak DMA1 Stream3 Handler");}
void Weak_DMA1_Stream4_IRQHandler(){doSomething("Weak DMA1 Stream4 Handler");}
void Weak_DMA1_Stream5_IRQHandler(){doSomething("Weak DMA1 Stream5 Handler");}
void Weak_DMA1_Stream6_IRQHandler(){doSomething("Weak DMA1 Stream6 Handler");}
void Weak_DMA1_Stream7_IRQHandler(){doSomething("Weak DMA1 Stream7 Handler");}



void Weak_DMA2_Stream0_IRQHandler(){doSomething("Weak DMA2 Stream0 Handler");}

//audio out dma interrupt
//void Weak_DMA2_Stream1_IRQHandler(){hurrdurrnothing("Weak DMA2 Stream1 Handler");}


void Weak_DMA2_Stream2_IRQHandler(){doSomething("Weak DMA2 Stream2 Handler");}
void Weak_DMA2_Stream3_IRQHandler(){doSomething("Weak DMA2 Stream3 Handler");}

//audio in dma interrupt
//void Weak_DMA2_Stream4_IRQHandler(){doSomething("Weak DMA2 Stream4 Handler");}

void Weak_DMA2_Stream5_IRQHandler(){doSomething("Weak DMA2 Stream5 Handler");}
void Weak_DMA2_Stream6_IRQHandler(){doSomething("Weak DMA2 Stream6 Handler");}
void Weak_DMA2_Stream7_IRQHandler(){doSomething("Weak DMA2 Stream7 Handler");}
