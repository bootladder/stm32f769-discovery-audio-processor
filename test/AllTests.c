#include "unity.h"

#include <stdbool.h>
#include <stdlib.h>
#include "FilterBlock.h"
#include "FilterTreeNode.h"

#include "steve_midi_input.h"
#include "MyFilterBlocks.h"
#include "helpers.h"
#include "logger.h"

#include "ContiguousBlockStorage.h"


///////////////////////////////////////////////////////
// TEST FILTERS TO USE
///////////////////////////////////////////////////////

float * memset_buffer_ConstantValue(FilterBlock_t * node, float * buf, uint32_t buffer_size){
  (void)node;
  for(uint32_t i=0;i<buffer_size;i++)
    buf[i] = 3.55;
  return buf;
}

float * add_1_to_buffer(FilterBlock_t * node, float * buf, uint32_t buffer_size){
  (void)node;
  for(uint32_t i=0;i<buffer_size;i++)
    buf[i] = buf[i] + 1.0;
  return buf;
}

float * add_3_to_buffer(FilterBlock_t * node, float * buf, uint32_t buffer_size){
  (void)node;
  for(uint32_t i=0;i<buffer_size;i++)
    buf[i] = buf[i] + 3.0;
  return buf;
}

////////////////////////////////////////////////////////////////////////////////
////     FILTER BLOCK             //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void TEST_FilterBlock_Setters_IsASetter(void)
{
  FilterBlock_t block;
  //use 8 bit values
  FilterBlock_SetCutoffFrequency(&block, 100);
  FilterBlock_SetBandwidth(&block, 100);
  FilterBlock_SetNumFilterTaps(&block, 100);
  FilterBlock_SetEnabled(&block, 100);
  TEST_ASSERT_EQUAL_INT(100, block.cutoff_freq);
  TEST_ASSERT_EQUAL_INT(100, block.bandwidth);
  TEST_ASSERT_EQUAL_INT(100, block.numFilterTaps);
  TEST_ASSERT_EQUAL_INT(100, block.enabled);
}


float * mock_compute_function(uint8_t numTaps, uint32_t cutoff, uint32_t bandwidth)
{
  (void)numTaps;
  (void)cutoff;
  (void)bandwidth;
  static float buf[200]; //coefficients
  buf[0] = 1.1;
  return buf;
}

void TEST_FilterBlock_ComputeFilterCoefficients_CallsComputeFunction_And_CopiesCoefficients()
{
  FilterBlock_t block;
  FilterBlock_SetCutoffFrequency(&block, 100);
  FilterBlock_SetBandwidth(&block, 100);
  FilterBlock_SetNumFilterTaps(&block, 100);
  FilterBlock_SetComputeFunction(&block, mock_compute_function);

  FilterBlock_ComputeFilterCoefficients(&block);

  TEST_ASSERT_EQUAL_FLOAT(1.1, block.filterCoefficients[0]);
}



void TEST_FilterBlock_ApplyFilter_ConstantValueFunction(void)
{
  FilterBlock_t block;
  block.filter = memset_buffer_ConstantValue;
  block.enabled = 1;

  float testbuf[AUDIO_BUFFER_SIZE];
  float * out_buf = FilterBlock_ApplyFilter(&block, testbuf, AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(3.55, out_buf[i]);
  }
}


void TEST_FilterBlock_ApplyFilter_BlockDisabled_SignalPassesThrough(void)
{
  FilterBlock_t block;
  block.filter = memset_buffer_ConstantValue;
  block.enabled = 0;

  float testbuf[AUDIO_BUFFER_SIZE];
  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    testbuf[i] = 77.7;
  }
  float * out_buf = FilterBlock_ApplyFilter(&block, testbuf, AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(77.7, out_buf[i]);
  }
}



bool mock_called = false;
void mock_midi_callback(FilterBlock_t * self, uint8_t value)
{
  (void)self;
  (void)value;
  mock_called = true;
}
void TEST_FilterBlock_RegisterMIDIMap_Registers(void)
{
  MIDI_Message_t message = {
    .status = 0x11,
    .id = 0x22,
    .value = 0x33,
  };
  MIDI_Message_t message2 = {
    .status = 0x22,
    .id = 0x33,
    .value = 0x44
  };

  FilterBlock_t block;

  block.midiMapEntries[0].message = message;
  block.midiMapEntries[0].callback = mock_midi_callback;

  block.midiMapEntries[1].message = message2;
  block.midiMapEntries[1].callback = mock_midi_callback;

  FilterBlock_SetPointerToFilterBlockArray(&block);

  mock_called = false;
  FilterBlock_RegisterMIDIMap(&block);
  MIDIMap_HandleMessage(message);

  TEST_ASSERT_TRUE_MESSAGE(mock_called, "Mock Not Called");

  mock_called = false;
  FilterBlock_RegisterMIDIMap(&block);
  MIDIMap_HandleMessage(message2);

  TEST_ASSERT_TRUE_MESSAGE(mock_called, "Mock Not Called");
}

void TEST_FilterBlock_IsValidInstance(void)
{
  static FilterBlock_t blocks[2];
  FilterBlock_SetPointerToFilterBlockArray(blocks);

  TEST_ASSERT_TRUE( FilterBlock_IsValidInstance(&blocks[0]));
  TEST_ASSERT_TRUE( FilterBlock_IsValidInstance(&blocks[1]));
  TEST_ASSERT_FALSE( FilterBlock_IsValidInstance((FilterBlock_t *)0x2));
  TEST_ASSERT_FALSE( FilterBlock_IsValidInstance((FilterBlock_t *)0x100000));
}


//This needs to be tested differently due to the sin
// and the arm dependency being inside MyFilterBlocks.c
// which will not compile in the test build

void TEST_FilterBlock_IdealBandpassFilter(void)
{//
 // FilterBlock_t block;
 // block.cutoff_freq = 1000;
 // block.bandwidth = 100;
 // block.numFilterTaps = 10;

 // uint32_t f_upper = 1000 + (100/2);
 // uint32_t f_lower = 1000 - (100/2);
 // float A = 2.0*3.14*f_upper/48000.0;  //2*pi*f/fsamp
 // float B = 2.0*3.14*f_lower/48000.0;

 // float coef0 = (A) - (B);
 // float coef1 = (A*sin(A*1.0)/(A*1.0)) - (B*sin(B*1.0)/(B*1.0));


 // FilterBlock_SetComputeFunction(&block, MyFilterBlocks_ComputeCoefficients_IdealBandpassFilter);
 // FilterBlock_ComputeFilterCoefficients(&block);

 // TEST_ASSERT_EQUAL_FLOAT(block.filterCoefficients[0], coef0);
 // TEST_ASSERT_EQUAL_FLOAT(block.filterCoefficients[1], coef1);
}


////////////////////////////////////////////////////////////////////////////////
///            FILTER TREE NODE     ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static FilterBlock_t TestingFilterBlocksSet[100] =
  {
    {} //zero
    ,
    {
      .filter = memset_buffer_ConstantValue,
      .enabled = 1,
    },
    {
      .filter = add_1_to_buffer,
      .enabled = 1,
    },

  };
void TEST_FilterTreeNode_Execute_Depth1_AppliesTheFilter(void)
{
  FilterTreeNode_SetPointerToFilterBlockArray(TestingFilterBlocksSet);
  FilterTreeNode_t tree;
  tree.filter_block_index = 1;
  tree.num_childs = 0;
  float testbuf[AUDIO_BUFFER_SIZE];

  float * out_buf = FilterTreeNode_Execute(&tree, testbuf,AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(3.55, out_buf[i]);
  }
}


void TEST_FilterTreeNode_Execute_Depth2_1Branch_Applies2Filters(void)
{
  FilterTreeNode_t tree;
  tree.filter_block_index = 1;
  tree.num_childs = 1;

  FilterTreeNode_t child0;
  child0.filter_block_index = 2;
  child0.num_childs = 0;

  tree.childs[0] = &child0;

  static float testbuf[AUDIO_BUFFER_SIZE];
  float * out_buf = FilterTreeNode_Execute(&tree, testbuf,AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(4.55, out_buf[i]);
  }
}

void TEST_FilterTreeNode_Execute_Depth3_1Branch_Applies3Filters(void)
{
  FilterTreeNode_t tree;
  tree.filter_block_index = 1;
  tree.num_childs = 1;

  FilterTreeNode_t child0;
  child0.filter_block_index = 2;
  child0.num_childs = 1;

  FilterTreeNode_t grandchild0;
  grandchild0.filter_block_index = 2;
  grandchild0.num_childs = 0;

  tree.childs[0] = &child0;
  child0.childs[0] = &grandchild0;



  static float testbuf[AUDIO_BUFFER_SIZE] = {0};
  float * out_buf = FilterTreeNode_Execute(&tree, testbuf,AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(5.55, out_buf[i]);
  }
}

void TEST_FilterTreeNode_Execute_Depth2_2Branches_Applies3Filters(void)
{
  FilterTreeNode_t tree;
  tree.filter_block_index = 1;
  tree.num_childs = 2;

  FilterTreeNode_t child0;
  child0.filter_block_index = 2;
  child0.num_childs = 0;

  FilterTreeNode_t child1;
  child1.filter_block_index = 2;
  child1.num_childs = 0;

  tree.childs[0] = &child0;
  tree.childs[1] = &child1;

  static float testbuf[AUDIO_BUFFER_SIZE] = {0};
  float * out_buf = FilterTreeNode_Execute(&tree, testbuf,AUDIO_BUFFER_SIZE);

  for(int i=0; i<AUDIO_BUFFER_SIZE; i++){
    TEST_ASSERT_EQUAL_FLOAT(9.10, out_buf[i]);
  }
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#include "MIDIMap.h"
#include "MIDIMessage.h"

void dummy_midi_callback(FilterBlock_t * self, uint8_t value){
  (void)self; (void)value;
}

void TEST_MIDIMap_Add_And_CheckForMatch(void)
{
  MIDI_Message_t message;
  message.status = 0x1;
  message.id = 0x1;
  message.value = 0x1;

  MIDIMapEntry_t entry;
  entry.message = message;
  entry.callback = dummy_midi_callback;
  FilterBlock_t * dummy = (FilterBlock_t *) 36; //dumym address

  MIDIMap_Add(dummy, &entry);

  bool result = MIDIMap_CheckForMatch(message);
  TEST_ASSERT_TRUE(result);

  MIDI_Message_t nonexistantmessage;
  nonexistantmessage.status = 0x2;
  nonexistantmessage.id = 0x2;
  nonexistantmessage.value = 0x2;

  result = MIDIMap_CheckForMatch(nonexistantmessage);
  TEST_ASSERT_FALSE(result);
}


static bool called = false;
static FilterBlock_t * mockSelf;
void mock_handle_midi_callback(FilterBlock_t * self, uint8_t value){
  (void)value;
  called = true;
  mockSelf = self;
}

void TEST_MIDIMap_HandleMessage_MessageMatches_CallsCallback_WithSelfParam(void)
{
  MIDI_Message_t message;
  message.status = 0x7;
  message.id = 0x7;
  message.value = 0x7;

  MIDIMapEntry_t entry;
  entry.message = message;
  entry.callback = mock_handle_midi_callback;
  FilterBlock_t * dummy = (FilterBlock_t *) 36; //dumym address

  MIDIMap_Add(dummy,&entry);

  //reset mock called
  called = false;

  MIDIMap_HandleMessage(message);
  TEST_ASSERT_TRUE_MESSAGE(called, "CALLBACK NOT CALLED");
  TEST_ASSERT_EQUAL_PTR(mockSelf, 36);


  MIDI_Message_t nonexistantmessage;
  nonexistantmessage.status = 0x2;
  nonexistantmessage.id = 0x2;
  nonexistantmessage.value = 0x2;


  //reset mock called
  called = false;

  MIDIMap_HandleMessage(nonexistantmessage);
  TEST_ASSERT_FALSE(called);

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void TEST_MIDIParser_ByteReceived_TestCases(void)
{
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');

  TEST_ASSERT_FALSE(MIDIParser_IsNewMessageAvailable());

  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived(0x80);
  MIDIParser_ByteReceived(20);
  MIDIParser_ByteReceived(1);
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');

  TEST_ASSERT_TRUE(MIDIParser_IsNewMessageAvailable());

  MIDI_Message_t message = MIDIParser_GetMessage();
  TEST_ASSERT_EQUAL_INT(message.status, 0x80);
  TEST_ASSERT_EQUAL_INT(message.id, 20);
  TEST_ASSERT_EQUAL_INT(message.value, 1);


  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived(0x90);
  MIDIParser_ByteReceived(30);
  MIDIParser_ByteReceived(10);
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');
  MIDIParser_ByteReceived('X');

  TEST_ASSERT_TRUE(MIDIParser_IsNewMessageAvailable());

  message = MIDIParser_GetMessage();
  TEST_ASSERT_EQUAL_INT(message.status, 0x90);
  TEST_ASSERT_EQUAL_INT(message.id, 30);
  TEST_ASSERT_EQUAL_INT(message.value, 10);
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void TEST_FloatToString(void)
{
  char str[100];
  float2string(3.14,str);
  TEST_ASSERT_EQUAL_INT8('3', str[0]);

  float2string(-3.14,str);
  TEST_ASSERT_EQUAL_INT8('-', str[0]);

  float2string(30.14,str);
  TEST_ASSERT_EQUAL_INT8('3', str[0]);
  TEST_ASSERT_EQUAL_INT8('0', str[1]);


  Logger_Debug("\n\nwhat\n\n");
  Logger_Debug(float2string(3.14,str)              );
  Logger_Debug(float2string(300.14,str)              );
  Logger_Debug(float2string(30.14123,str)            );
  Logger_Debug(float2string(3.141232,str)            );
  Logger_Debug(float2string(0.1131313131314,str)     );
  Logger_Debug(float2string(213124512.14,str)        );
  Logger_Debug(float2string(12345678.14,str)        );
  Logger_Debug(float2string(1234567.14,str)        );
  Logger_Debug(float2string(123456.14,str)        );
  Logger_Debug(float2string(123124.123141,str)       );

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void TEST_ContiguousBlockStorage_StoreBlock_1Block_AppearsAtTheEnd(void)
{
  float block[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    block[i] = 3.3;
  }
  ContiguousBlockStorage_StoreBlock(block);

  float * start_of_buffer = ContiguousBlockStorage_Beginning();
  int latest_block_index = CONTIGUOUSSTORAGE_BLOCK_SIZE*(CONTIGUOUSSTORAGE_NUM_BLOCKS-1);
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++)
    {
      TEST_ASSERT_EQUAL_FLOAT(start_of_buffer[latest_block_index + i] , block[i]);
    }
}

void TEST_ContiguousBlockStorage_StoreBlock_2Blocks_SecondBlockAppearsAtTheEnd_SecondBlockIsAfterFirstBlock(void)
{
  float firstblock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  float secondblock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    firstblock[i] = 3.3;
  }
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    secondblock[i] = 6.6;
  }


  ContiguousBlockStorage_StoreBlock(firstblock);
  ContiguousBlockStorage_StoreBlock(secondblock);

  ContiguousBlockStorage_Print();

  float * start_of_buffer = ContiguousBlockStorage_Beginning();

  //first block to be stored appears as second to last block
  //ie. 2rd index

  int second_latest_block_index =
    CONTIGUOUSSTORAGE_BLOCK_SIZE*(CONTIGUOUSSTORAGE_NUM_BLOCKS-2);

  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++)
    {
      TEST_ASSERT_EQUAL_FLOAT_MESSAGE(start_of_buffer[second_latest_block_index + i] , firstblock[i], "expecting firstblock 3.3");
    }

  //second block to be stored appears as last block
  //ie. 3rd index

  int latest_block_index =
    CONTIGUOUSSTORAGE_BLOCK_SIZE*(CONTIGUOUSSTORAGE_NUM_BLOCKS-1);

  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++)
    {
      TEST_ASSERT_EQUAL_FLOAT_MESSAGE(start_of_buffer[latest_block_index + i] , secondblock[i], "test number 2");
    }

}


void TEST_ContiguousBlockStorage_StoreBlock_3Blocks_ThirdBlockIsAtTheEnd(void){

  float firstblock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  float secondblock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  float thirdblock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    firstblock[i] = 3.3;
    secondblock[i] = 6.6;
    thirdblock[i] = 9.9;
  }


  ContiguousBlockStorage_StoreBlock(firstblock);
  ContiguousBlockStorage_StoreBlock(secondblock);
  ContiguousBlockStorage_StoreBlock(thirdblock);

  ContiguousBlockStorage_Print();

  //last block to be stored appears as last block
  //ie. 3rd index
  float * start_of_buffer = ContiguousBlockStorage_Beginning();

  int latest_block_index =
    CONTIGUOUSSTORAGE_BLOCK_SIZE*(CONTIGUOUSSTORAGE_NUM_BLOCKS-1);

  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++)
    {
      TEST_ASSERT_EQUAL_FLOAT_MESSAGE(start_of_buffer[latest_block_index + i] , thirdblock[i], "test number 3");
    }

}


void TEST_ContiguousBlockStorage_StoreBlock_LotsOfBlocks_LastBlockIsAtTheEnd(void)
{

  float allOfTheOtherBlocks[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  float lastBlock[CONTIGUOUSSTORAGE_BLOCK_SIZE];
  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++){
    allOfTheOtherBlocks[i] = 3.3;
    lastBlock[i] = 9.9;
  }

  printf("\nfuck");
  fflush(stdout);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  printf("\nfuck");
  fflush(stdout);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  printf("\nfuck");
  fflush(stdout);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  ContiguousBlockStorage_StoreBlock(lastBlock);


  ContiguousBlockStorage_Print();

  //last block to be stored appears as last block
  //ie. 3rd index
  float * start_of_buffer = ContiguousBlockStorage_Beginning();

  int latest_block_index =
    CONTIGUOUSSTORAGE_BLOCK_SIZE*(CONTIGUOUSSTORAGE_NUM_BLOCKS-1);

  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE; i++)
    {
      TEST_ASSERT_EQUAL_FLOAT_MESSAGE(start_of_buffer[latest_block_index + i] , lastBlock[i], "test number 3");
    }

}

void TEST_ContiguousBlockStorage_StoreBlock_LotsOfBlocks_WholeBlockMatchesEntirely(void)
{
  float allOfTheOtherBlocks[CONTIGUOUSSTORAGE_BLOCK_SIZE];

  int lotsOfBlocksSize = 79;

  for(int i=0;i<lotsOfBlocksSize;i++){
    for(int j=0; j<CONTIGUOUSSTORAGE_BLOCK_SIZE; j++){
      allOfTheOtherBlocks[j] = (float)i;
    }
    ContiguousBlockStorage_StoreBlock(allOfTheOtherBlocks);
  }

  ContiguousBlockStorage_Print();

  //last block to be stored appears as last block
  //ie. 3rd index
  float * start_of_buffer = ContiguousBlockStorage_Beginning();

  //buffer contents are equal to 60, .. 64

  float bufferContent;

  bufferContent = (float)lotsOfBlocksSize - CONTIGUOUSSTORAGE_NUM_BLOCKS;
  Logger_Printf("bufferContent is %f\n", bufferContent);

  for(int i=0; i<CONTIGUOUSSTORAGE_BLOCK_SIZE*CONTIGUOUSSTORAGE_NUM_BLOCKS; i++){
    TEST_ASSERT_EQUAL_FLOAT_MESSAGE(start_of_buffer[i] , bufferContent + (i/CONTIGUOUSSTORAGE_BLOCK_SIZE) , "test number 3");
  }

  ContiguousBlockStorage_PrintTheWholeBuffer();
}



void TEST_ContiguousBlockStorage_Size_IsCorrect(void)
{
  uint32_t size = ContiguousBlockStorage_BufferSize();
  TEST_ASSERT_EQUAL_INT(CONTIGUOUSSTORAGE_BLOCK_SIZE*CONTIGUOUSSTORAGE_NUM_BLOCKS, size);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(void)
{
  UNITY_BEGIN();


  RUN_TEST(TEST_FilterBlock_Setters_IsASetter);
  RUN_TEST(TEST_FilterBlock_ComputeFilterCoefficients_CallsComputeFunction_And_CopiesCoefficients);
  RUN_TEST(TEST_FilterBlock_RegisterMIDIMap_Registers);
  RUN_TEST(TEST_FilterBlock_ApplyFilter_ConstantValueFunction);
  RUN_TEST(TEST_FilterBlock_ApplyFilter_BlockDisabled_SignalPassesThrough);
  RUN_TEST(TEST_FilterBlock_IsValidInstance);
  RUN_TEST(TEST_FilterBlock_IdealBandpassFilter);


  RUN_TEST(TEST_FilterTreeNode_Execute_Depth1_AppliesTheFilter);
  RUN_TEST(TEST_FilterTreeNode_Execute_Depth2_1Branch_Applies2Filters);
  RUN_TEST(TEST_FilterTreeNode_Execute_Depth3_1Branch_Applies3Filters);
  RUN_TEST(TEST_FilterTreeNode_Execute_Depth2_2Branches_Applies3Filters);


  RUN_TEST(TEST_MIDIMap_Add_And_CheckForMatch);
  RUN_TEST(TEST_MIDIMap_HandleMessage_MessageMatches_CallsCallback_WithSelfParam);


  RUN_TEST(TEST_MIDIParser_ByteReceived_TestCases);

  RUN_TEST(TEST_FloatToString);

  RUN_TEST(TEST_ContiguousBlockStorage_StoreBlock_1Block_AppearsAtTheEnd);
  RUN_TEST(TEST_ContiguousBlockStorage_StoreBlock_2Blocks_SecondBlockAppearsAtTheEnd_SecondBlockIsAfterFirstBlock);
  RUN_TEST(TEST_ContiguousBlockStorage_StoreBlock_3Blocks_ThirdBlockIsAtTheEnd);
  RUN_TEST(TEST_ContiguousBlockStorage_StoreBlock_LotsOfBlocks_LastBlockIsAtTheEnd);
  RUN_TEST(TEST_ContiguousBlockStorage_StoreBlock_LotsOfBlocks_WholeBlockMatchesEntirely);
  RUN_TEST(TEST_ContiguousBlockStorage_Size_IsCorrect);

  return UNITY_END();
}



void ASSERT(bool cond){
  if(false == cond)
    {
      Logger_Printf("\n\n\nassert failed\n\n\n");fflush(stdout);
      exit(1);
    }

}
